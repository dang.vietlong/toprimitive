Object.prototype[Symbol.toPrimitive] = function (hint) {
  console.log(`Convert type: ${hint}`)
  if (hint === 'number') {
    return toNumber(this)
  } else if (hint === 'string') {
    return toString(this)
  }

  return toNumber(this)

  function isPrimitive(value) {
    return typeof value !== 'object'
  }

  function toString(value) {
    if (isPrimitive(value.toString())) return value.toString()
    if (isPrimitive(value.valueOf())) return value.valueOf()
    throw new TypeError(`Cannot convert object to primitive value`)
  }

  function toNumber(value) {
    if (isPrimitive(value.valueOf())) return value.valueOf()
    if (isPrimitive(value.toString())) return value.toString()
    throw new TypeError(`Cannot convert object to primitive value`)
  }
}

console.log(String({}))
console.log(typeof 5)

console.log(+{})
console.log(Number(5))

console.log([] + {})
console.log({} + {})
